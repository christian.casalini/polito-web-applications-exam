# Exam #2: "Generatore di meme"
## Student: s281823 CASALINI CHRISTIAN 

## React Client Application Routes

- Route `/`: the main page, with different content depending on user's authentication
  - For non-authenticated user:
    - a collection of puclic memes previews
    - a 'Login' button on the right side of the navigation bar
  - For authenticated user:
    - a collection of previews of all the memes (private and public)
    - below each meme there are two buttons and one icon:
      - an icon that shows the visibility of the meme (private / public)
      - a button to perform a copy of the meme
      - a button to delete the meme
    - a 'Create new meme' button, horizontally centered in the page, just before the collection of previews
    - authenticated user's name on the right side of the navigation bar
    - a drop down menu, on the right of user's name in navigation bar, that contains the 'Logout' button
  - Common:
    - Navigation bar on top, with the app's name on the left
    - Web application's slogan
    - Title and Creator for each meme's preview

- Route `/new-meme`: a modal that lets the user create a new meme; it contains
  - A form to set the title of the meme
  - Three forms to insert the text fields allowed by the chosen template, disabled if needed
    - e.g. if the template allows only two text fields, the form for the third text field is disabled
    - as an implementation choice I keep the inserted text in the forms when switching template (no specs were given about this)
  - A palette of five colors, to choose the color of the text (color is the same for all the three text fields)
  - A check form to set visibility (private if checked)
  - A selection form to choose text's font (six fonts available)
  - An horizontally scrollable container, containing a collection of selectable previews of all the available templates
  - A live meme preview, that shows the image and the inserted text with all the chosen properties as user changes them
  
- Route `/copy-meme`: a modal that lets the user create a copy of an existing meme; it differs from `/new-meme` for
  - No collection of templates (as the template cannot be changed when performing a copy)
  - Visibility check form can be disabled, if visibility cannot be changed (performing a copy of a private meme created by another creator)

- Route `/meme-detail`: a modal that shows the detail of a meme; it contains
  - A complete preview of the meme
  - A column containing various details about the meme
    - Title
    - Text font
    - Text color
    - Creator's name
    - Visibility

    I chose to list all these details even if not required from specs because, having the complete previews with title and creator in the main page, it would not make so much sense to have a dedicated modal with the exact same content.

- Route `/login`: a modal that lets the user perform the login; it contains
  - A form for the email
  - A form for the password
  - A 'Login' button to submit credentials

  There is no 'Discard' button since the redirection to the main page can be achieved by pressing the 'Esc' key or by clicking outside the modal itself.
<br><br>

 
## API Server

### User APIs
- POST `/api/sessions`
  - Description: authenticate the user who is trying to login
  - Request body: credentials of the user who is trying to login
    ```
    {
      "username" : "user1@test.it",
      "password" : "esame2memegitPr0bitU1"
    }
    ```
  - Response: `200 (OK)`
  - Response body:
    ```
    {
      "id": 1,
      "username": "user1@test.it",
      "name": "B3stMemeCr3at0r"
    }
    ```
  - Error responses: `500 Internal Server Error` (generic error), `401 Unauthorized User` (login failed) <br><br>

- DELETE `/api/sessions/current`
  - Description: logout current user
  - Request body: *None*
  - Response: `200 (OK)`
  - Response body: *None*
  - Error responses: `500 Internal Server Error` (generic error), 401 `Unauthorized User` (no user is logged in) <br><br>

- GET `/api/sessions/current`
  - Description: check if a user is authenticated (logged in)
  - Request body: *None*
  - Response: `200 (OK)`
  - Response body: authenticated user
    ```
    {
      "id": 1,
      "username": "user1@test.it",
      "name": "B3stMemeCr3at0r"
    }
    ```
  - Error responses: `500 Internal Server Error` (generic error), 401 `Unauthorized User` (no user is logged in) <br><br>


### Meme APIs
- GET `/api/memes/public`
  - Description: get all the **public** memes
  - Request body: *None*
  - Response: `200 (OK)`
  - Response body:
    ```
    [
    ...
    {
      "id": 3,
      "title": "Potter car meme's title",
      "parent_id": -1,
      "creator_id": 3,
      "text_color": "#ffffff",
      "font": "Helvetica",
      "private": 0,
      "text1": "Review one more time",
      "text2": "git tag final",
      "text3": "Me committing AW1 exam project",
      "template_id": 2,
      "creator_name": "MrH1de"
    }, {
    "id": 5,
    "title": "Car drift meme's title",
    "parent_id": -1,
    "creator_id": 3,
    "text_color": "#ffffff",
    "font": "Helvetica",
    "private": 0,
    "text1": "Showing me images I searched for",
    "text2": "Showing me images I didn't search for",
    "text3": "Google images",
    "template_id": 5,
    "creator_name": "MrH1de"
    },
    ...
    ]
    ```
  - Error responses: `500 Internal Server Error` (generic error)<br><br>

- GET `/api/memes`
  - Description: get all the memes (**private** *and* **public**)
  - Request body: *None*
  - Response: `200 (OK)`
  - Response body:
    ```
    [
    ...
    {
      "id": 5,
      "title": "Car drift meme's title",
      "parent_id": -1,
      "creator_id": 3,
      "text_color": "#ffffff",
      "font": "Helvetica",
      "private": 0,
      "text1": "Showing me images I searched for",
      "text2": "Showing me images I didn't search for",
      "text3": "Google images",
      "template_id": 5,
      "creator_name": "MrH1de"
    }, {
      "id": 6,
      "title": "Car bottle meme's title",
      "parent_id": -1,
      "creator_id": 1,
      "text_color": "#ffffff",
      "font": "Helvetica",
      "private": 1,
      "text1": "When code runs perfectly at first try",
      "text2": "",
      "text3": "",
      "template_id": 6,
      "creator_name": "B3stMemeCr3at0r"
    },
    ...
    ]
    ```
  - Error responses: `500 Internal Server Error` (generic error), 401 `Unauthorized User` (no user is logged in) <br><br>

- POST `/api/memes`
  - Description: add a new meme in the DB
  - Request body: meme to add
    ```
    {
      "title": "Car bottle meme's title",
      "parent_id": -1,
      "creator_id": 1,
      "text_color": "#ffffff",
      "font": "Helvetica",
      "private": 1,
      "text1": "When code runs perfectly at first try",
      "text2": "",
      "text3": "",
      "template_id": 6
    }
    ```
  - Response: `200 (OK)`
  - Response body:
    ```
    {
      "id": 38,
      "title": "Car bottle meme's title",
      "parent_id": -1,
      "creator_id": 1,
      "text_color": "#ffffff",
      "font": "Helvetica",
      "private": 1,
      "text1": "When code runs perfectly at first try",
      "text2": "",
      "text3": "",
      "template_id": 6
    }
    ```
  - Error responses: `503 Service Unavailable` (Database error during the creation of new meme), `401 Unauthorized User` (unauthenticated user), `422 Unprocessable Entity` (a validation related to request body failed) <br><br>

- DELETE `/api/memes/<id>`
  - Description: delete from the DB an existing meme identified by its *id*
  - Request body: *None*
  - Response: `200 (OK)`
  - Response body: empty object
  - Error responses: `503 Internal Server Error` (Database error during the deletion of meme), `401 Unauthorized User` (unauthenticated user), `422 Unprocessable Entity` (an invalid *id* was passed OR trying to delete a meme that was not created by the authenticated user), `404 Not Found` (no meme with given *id* in the DB)<br><br>

### Template APIs
- GET `/api/templates`
  - Description: get all the templates
  - Request body: *None*
  - Response: `200 (OK)`
  - Response body:
    ```
    [{
      "id": 1,
      "source": "Wall.gif",
      "texts_n": 1,
      "text1_pos": "wallT1",
      "text2_pos": "",
      "text3_pos": ""
    }, {
      "id": 2,
      "source": "PotterCar.png",
      "texts_n": 3,
      "text1_pos": "potterCarT1",
      "text2_pos": "potterCarT2",
      "text3_pos": "potterCarT3"
    },
    ...
    ]
    ```
  - Error responses: `500 Internal Server Error` (generic error)<br><br>

## Database Tables

- Table `users` :  
  - *id*
  - *email*
  - *name*
  - *hash* (generated with bcrypt starting from user's password)
- Table `memes` :
  - *id*
  - *title*
  - *parent_id*   ( -1 if meme is not a copy )
  - *creator_id*
  - *text_color*
  - *font*
  - *private*
  - *text1*
  - *text2*
  - *text3*
  - *template_id*
- Table `templates` :
  - *id*
  - *source*
  - *texts_n*
  - *text1_pos*
  - *text2_pos*
  - *text3_pos*
<br><br>

## Main React Components

- `MGCollectionView` (in `MGCollectionView.js`): shows the collection of memes previews in the main page (both for authenticated and non-authenticated user)
- `MGCollectionViewCell` (in `MGCollectionViewCell.js`): it represents the complete preview of a meme; it contains the title, an `MGMemeView`, creator's name, an icon that represents meme's visibility and buttons for copy and delete (buttons are visible only if user is authenticated)
- `MGMemeView` (in `MGMemeView.js`): is the effective preview of the meme, shows the base image and the text fields properly positioned over it
- `MGLogin` (in `MGLogin.js`): lets the user perform login
- `MGCreationView` (in `MGCreationView.js`): is the modal presented both for creation and copy of a meme (if performing a copy, it does not allow to change meme's template)
- `MGDetailView` (in `MGDetailView.js`): shows the detail of a meme (complete preview, title, creator's name and other details)
- `MGNavbar` (in `MGNavbar.js`): contains App's title on the left and, on the right, a button that gets the user to Login or, if already logged in, allows to perform logout
<br><br>

## Screenshot

![Screenshot](./Screenshot.png)

<br><br>

## Users Credentials

|     email     |        password       |       name      |
|---------------|-----------------------|-----------------|
| user1@test.it | esame2memegitPr0bitU1 | B3stMemeCr3at0r |
| user2@test.it | esame2memegitPr0bitU2 | MemeG3nius      |
| user3@test.it | esame2memegitPr0bitU3 | MrH1de          |
| user4@test.it | esame2memegitPr0bitU4 | SuperMeme       |


## Memes listed by creator
- B3stMemeCr3at0r (user1@test.it)
  - Sheldon meme 01
  - Wall meme 01
  - Medal meme 02 (copy of Medal meme 01)
- MemeG3nius (user2@test.it)
  - PotterCar meme 01
  - CarDrift meme 01
  - Sheldon meme 02 (copy of Sheldon meme 01)
- MrH1de (user3@test.it)
  - CarBottle meme 01
  - Ross meme 01
  - PotterCar meme 02 (copy of PotterCar meme 01)
- SuperMeme (user4@test.it)
  - Medal meme 01
  - Calculating meme 01
  - CarBottle meme 02 (copy of CarBottle meme 01)
