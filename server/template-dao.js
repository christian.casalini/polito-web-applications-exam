'use strict';

/* Data Access Object (DAO) module for accessing templates */

const db = require('./db');

// get templates
exports.listTemplates = () => {
  return new Promise((resolve, reject) => {
    const sql = 'SELECT * FROM templates';
    db.all(sql, (err, rows) => {
      if (err) {
        reject(err);
        return;
      }
      const templates = [...rows];
      
      resolve( templates );
    });
  });
};

// get the template identified by given id
exports.getTemplate = (id) => {
  return new Promise((resolve, reject) => {
    const sql = 'SELECT * FROM templates WHERE id=?';
    db.get(sql, [id], (err, row) => {
      if (err) {
        reject(err);
        return;
      }
      if (row == undefined) {
        resolve({ error: 'Template not found.' });
      } else {
        const template = { ...row }; 
        resolve(template);
      }
    });
  });
};

