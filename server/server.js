'use strict';

const express = require('express');
const morgan = require('morgan'); // logging middleware
const { check, validationResult, oneOf } = require('express-validator'); // validation middleware
const passport = require('passport'); // auth middleware
const LocalStrategy = require('passport-local').Strategy; // username and password for login
const session = require('express-session'); // enable sessions

const memeDao = require('./meme-dao'); // module for accessing the memes in the DB
const userDao = require('./user-dao'); // module for accessing the users in the DB
const templateDao = require('./template-dao'); // module for accessing the templates in the DB


/***********************************************************************/
/*                           Set up Passport                           */
/***********************************************************************/

// set up the "username and password" login strategy
// by setting a function to verify username and password
passport.use(new LocalStrategy(
  function (username, password, done) {
    userDao.getUser(username, password).then((user) => {
      if (!user)
        return done(null, false, { message: 'Incorrect username and/or password.' });

      return done(null, user);
    })
  }
));

// serialize and de-serialize the user (user object <-> session)
// we serialize the user id and we store it in the session: the session is very small in this way
passport.serializeUser((user, done) => {
  done(null, user.id);
});

// starting from the data in the session, we extract the current (logged-in) user
passport.deserializeUser((id, done) => {
  userDao.getUserById(id)
    .then(user => {
      done(null, user); // this will be available in req.user
    }).catch(err => {
      done(err, null);
    });
});

const errorFormatter = ({ location, msg, param, value, nestedErrors }) => {
  // Format express-validate errors as strings
  return `${location}[${param}]: ${msg}`;
};

// init express
const app = new express();
const PORT = 3001;

// set-up the middlewares
app.use(morgan('dev'));
app.use(express.json());

// set up the session
app.use(session({
  // by default, Passport uses a MemoryStore to keep track of the sessions
  secret: '- Exigua pars est vitae qua vivimus. Ceterum quidem omne spatium non vita sed tempus est. - (Seneca, De brevitate vitae 2, 2, 9)',
  resave: false,
  saveUninitialized: false
}));

// then, init passport
app.use(passport.initialize());
app.use(passport.session());



/***********************************************************************/
/*                         Custom middlewares                          */
/***********************************************************************/

// Custom middleware: check if a given request is coming from an authenticated user
const isLoggedIn = (req, res, next) => {
  if (req.isAuthenticated())
    return next();

  return res.status(401).json({ error: 'Not authenticated' });
}

/* Custom middleware: check, if the meme is a copy, that the copied meme respects the specs
 * Specs: 1) template must be the same of parent-meme's one
 *        2) if parent-meme belongs to another user and its visibility is PRIVATE,
 *           the copied meme (the one trying to POST) must be PRIVATE as well
 */
const isNewMemeOrValidCopy = async (req, res, next) => {
  if (req.body.parent_id === -1)
    return next();
  
  try {
    const parent = await memeDao.getMeme(req.body.parent_id);
    if (parent.error)
      return res.status(404).json(parent);
    
      const sameTemplate = parent.template_id === req.body.template_id;
      const protectedVisChanged = parent.creator_id != req.body.creator_id && parent.private === 1 && parent.private !== req.body.private;
      if (sameTemplate && !protectedVisChanged)
        return next();
  } catch (err) {
    return res.status(500).end();
  }

  return res.status(422).json({ error: 'Meme is a copy that does not respect specs about visibility.' });
}

// Custom middleware: check that new meme's creator matches the current authenticated user
const creatorMatchesUser = (req, res, next) => {
  // Check that creator_id is present in req.body
  if (!req.body.creator_id)
    return res.status(422).json({ error: 'The meme must have a creator.' });

  if (req.body.creator_id === req.user.id)
    return next();

  return res.status(422).json({ error: 'Creator does not correspond to current user.' });
}


// Custom middleware: check that meme contains only texts allowed by the template it refers to
//                    this check also avoids POST of a meme referring to template that does not exist in DB
const memeConformsToTemplate = async (req, res, next) => {
  try {
    const template = await templateDao.getTemplate(req.body.template_id);
    if (template.error)
      return res.status(404).json(template);
    
    let valid = true;
    if (template.texts_n < 2 && req.body.text2.length > 0)
      valid = false;

    if (template.texts_n < 3 && req.body.text3.length > 0)
      valid = false;
      
    if (valid)
      return next();
  } catch (err) {
      return res.status(500).json(err).end();
  }

  res.status(422).json({ error: 'Meme does not conform to its template.' });
}

// Custom middleware: verify that an existing meme belongs to the current authenticated user
const memeBelongsToUser = async (req, res, next) => {
  // Check that meme's id is present in req.params
  if (!req.params.id)
    return res.status(422).json({ error: 'A valid id should be passed as parameter.' });

    try {
      const meme = await memeDao.getMeme(req.params.id);
      if (meme.error)
        return res.status(404).json(meme);
      
      const belongs = meme.creator_id === req.user.id;
      if (belongs)
        return next();
    } catch (err) {
      return res.status(500).end();
    }

  return res.status(422).json({ error: 'Trying to delete a meme created by another user.' });
}

/***********************************************************************/
/*                              MEME APIs                              */
/***********************************************************************/

// GET /api/memes - Get all the memes in the DB if a valid user is authenticated
app.get('/api/memes',
  isLoggedIn,
  (req, res) => {
      memeDao.listAllMemes()
             .then(memes => res.json(memes))
             .catch(() => res.status(500).end());
});

// GET /api/memes/public - Get all the public memes in the DB
app.get('/api/memes/public',
  (req, res) => {
      memeDao.listPublicMemes()
             .then(memes => res.json(memes))
             .catch(() => res.status(500).end());
});

// POST /api/memes - Add a new meme to the DB
app.post('/api/memes', isLoggedIn, creatorMatchesUser, isNewMemeOrValidCopy, memeConformsToTemplate,
[ check(['template_id', 'private', 'parent_id']).isInt(),
  check('private').isInt().isIn([0, 1]),
  check('title').isLength({ min: 1, max:35 }),
  check('font').isIn(['Helvetica', 'Times New Roman', 'Courier New', 'Bradley Hand', 'Brush Script MT', 'Cursive']),
  check('text_color').isIn(['#000000', '#ffffff', '#fcba03', '#5ce074', '#5d88d9']),
  check(['text1', 'text2', 'text3']).isString().isLength({max: 200}),
  oneOf([ check('text1').notEmpty(), check('text2').notEmpty(), check('text3').notEmpty()]) ], 
async (req, res) => {
  const errors = validationResult(req).formatWith(errorFormatter); // format error message
  if (!errors.isEmpty()) {
    return res.status(422).json({ error: errors.array().join(", ")  }); // error message is a single string with all error joined together
  }

  const meme = {
    title: req.body.title,
    parent_id: req.body.parent_id,
    creator_id: req.body.creator_id,
    text_color: req.body.text_color,
    font: req.body.font,
    private: req.body.private,
    text1: req.body.text1,
    text2: req.body.text2,
    text3: req.body.text3,
    template_id: req.body.template_id
  };  

  try {
    const result = await memeDao.createMeme(meme);
    res.json(result); 
  } catch (err) {
    res.status(503).json({ error: `Database error during the creation of new meme: ${err}.` }); 
  }
});

// DELETE /api/memes/<id>  - Delete an existing meme from the DB
app.delete('/api/memes/:id', isLoggedIn, [ check('id').isInt() ], memeBelongsToUser,
  async (req, res) => {
    const errors = validationResult(req).formatWith(errorFormatter); // format error message
    if (!errors.isEmpty()) {
      return res.status(422).json({ error: errors.array().join(", ") }); // error message is a single string with all error joined together
    }

    try {
      await memeDao.deleteMeme(req.user.id, req.params.id);
      res.status(200).json({});
    } catch (err) {
      res.status(503).json({ error: `Database error during the deletion of meme ${req.params.id}` });
    }
  });



/***********************************************************************/
/*                            TEMPLATE APIs                            */
/***********************************************************************/

// GET /api/templates - Get all the templates in the DB
app.get('/api/templates',
  (req, res) => {
      templateDao.listTemplates()
      .then(templates => res.json(templates))
      .catch(() => res.status(500).end());
});



/***********************************************************************/
/*                              USER APIs                              */
/***********************************************************************/

// Login --> POST /sessions - Authenticate the user who is trying to login
app.post('/api/sessions', function (req, res, next) {
  passport.authenticate('local', (err, user, info) => {
    if (err)
      return next(err);
    if (!user) {
      // display wrong login messages
      return res.status(401).json(info);
    }
    // success, perform the login
    req.login(user, (err) => {
      if (err)
        return next(err);

      // req.user contains the authenticated user, we send all the user info back
      // this is coming from userDao.getUser()
      return res.json(req.user);
    });
  })(req, res, next);
});

// Logout --> DELETE /sessions/current 
app.delete('/api/sessions/current', (req, res) => {
  req.logout();
  res.end();
});

// GET /sessions/current - Check whether a user is logged in or not
app.get('/api/sessions/current', (req, res) => {
  if(req.isAuthenticated())
    res.status(200).json(req.user);
  else
    res.status(401).json({error: 'Unauthenticated user!'});
});



/***********************************************************************/
/*                 Other express-related instructions                  */
/***********************************************************************/

// Activate the server
app.listen(PORT, () => console.log(`Server running on http://localhost:${PORT}/`));

