'use strict';

/* Data Access Object (DAO) module for accessing memes */

const db = require('./db');

// get all memes
exports.listAllMemes = () => {
  return new Promise((resolve, reject) => {
    const sql = 'SELECT memes.*, users.name AS creator_name \
                 FROM memes INNER JOIN users ON memes.creator_id = users.id';
    db.all(sql, (err, rows) => {
      if (err) {
        reject(err);
        return;
      }
      const memes = [...rows];
      
      resolve( memes );
    });
  });
};

// get public memes
exports.listPublicMemes = () => {
  return new Promise((resolve, reject) => {
    const sql = 'SELECT memes.*, users.name AS creator_name \
                 FROM memes INNER JOIN users ON memes.creator_id = users.id\
                 WHERE private = 0';
    db.all(sql, (err, rows) => {
      if (err) {
        reject(err);
        return;
      }
      const memes = [...rows];
      
      resolve( memes );
    });
  });
};

// get the meme identified by given id
exports.getMeme = (id) => {
  return new Promise((resolve, reject) => {
    const sql = 'SELECT * FROM memes WHERE id=?';
    db.get(sql, [id], (err, row) => {
      if (err) {
        reject(err);
        return;
      }
      if (row == undefined) {
        resolve({ error: 'Meme not found.' });
      } else {
        const meme = { ...row }; 
        resolve(meme);
      }
    });
  });
};

// add a new meme
// the meme's id is added automatically by the DB
exports.createMeme = (meme) => {
  return new Promise((resolve, reject) => {
    const sql = 'INSERT INTO memes (title, parent_id, creator_id, text_color, font, private, text1, text2, text3, template_id) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)';
    db.run(sql, [meme.title, meme.parent_id, meme.creator_id, meme.text_color, meme.font, meme.private, meme.text1, meme.text2, meme.text3, meme.template_id], function (err) {
      if (err) {
        reject(err);
        return;
      }
      resolve(exports.getMeme(this.lastID));
    });
  });
};

// delete an existing meme
exports.deleteMeme = (userID, memeID) => {
  return new Promise((resolve, reject) => {
    const sql = 'DELETE FROM memes WHERE id = ? and creator_id = ?';
    db.run(sql, [memeID, userID], (err) => {
      if (err) {
        reject(err);
        return;
      } else
        resolve(null);
    });
  });
}

