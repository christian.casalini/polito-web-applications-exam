/**
 * All the API calls
 */

const BASEURL = '/api';


/***********************************************************************/
/*                            TEMPLATE APIs                            */
/***********************************************************************/

async function getTemplates() {
  return getJson(
    fetch(BASEURL + '/templates')
  ).then(json => {
    return json.map((template) => Object.assign({}, template))
  })
}


/***********************************************************************/
/*                              MEME APIs                              */
/***********************************************************************/

async function getMemes(authenticated) {
  // Here I chose to have just 1 API function for both authenticated and unauthenticated requests
  // just to simplify a bit the logic in App.js (I don't really need two dedicated functions)
  return getJson(
    fetch(BASEURL + ( authenticated ? '/memes' : '/memes/public') )
  ).then(json => {
    return json.map((meme) => Object.assign({}, meme))
  })
}

function addMeme(meme) {
  return getJson(
    fetch(BASEURL + "/memes", {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ ...meme })
    })
  )
}

function deleteMeme(meme) {
  return getJson(
    fetch(BASEURL + "/memes/" + meme.id, {
      method: 'DELETE'
    })
  )
}


/***********************************************************************/
/*                              USER APIs                              */
/***********************************************************************/

async function logIn(credentials) {
  let response = await fetch('/api/sessions', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(credentials),
  });
  if (response.ok) {
    const user = await response.json();
    return user;
  }
  else {
    try {
      const errDetail = await response.json();
      throw errDetail;
    }
    catch (err) {
      throw err;
    }
  }
}

async function logOut() {
  const response = await fetch('/api/sessions/current', { method: 'DELETE' });
  if (!response.ok) {
    try {
      const errDetail = await response.json();
      throw errDetail;
    }
    catch (err) {
      throw err;
    }
  }
}

async function getUserInfo() {
  const response = await fetch(BASEURL + '/sessions/current');
  const userInfo = await response.json();
  if (response.ok) {
    return userInfo;
  } else {
    throw userInfo;  // an object with the error coming from the server, mostly unauthenticated user
  }
}


/***********************************************************************/
/*                              Utilities                              */
/***********************************************************************/

function getJson(httpResponsePromise) {
  return new Promise((resolve, reject) => {
    httpResponsePromise
      .then((response) => {
        if (response.ok) {

          // always return {} from server, never null or non json, otherwise it will fail
          response.json()
            .then(json => resolve(json))
            .catch(err => reject({ error: "Cannot parse server response" }))

        } else {
          // analyze the cause of error
          response.json()
            .then(obj => reject(obj)) // error msg in the response body
            .catch(err => reject({ error: "Cannot parse server response" })) // something else
        }
      })
      .catch(err => reject({ error: "Cannot communicate" })) // connection error
  });
}

const API = { addMeme, getMemes, deleteMeme, getTemplates, logIn, logOut, getUserInfo }
export default API;
