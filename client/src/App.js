import './App.css';

import 'bootstrap/dist/css/bootstrap.css';
import MGNavbar from './components/MGNavbar.js';
import MGCollectionView from './components/MGCollectionView';
import MGCreationView from './components/MGCreationView';
import MGDetailView from './components/MGDetailView';
import { LoginForm } from './components/MGLogin';
import { Container, Row, Button, Form, Toast } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faGrinSquintTears } from '@fortawesome/free-solid-svg-icons'
import { useState, useEffect } from 'react';
import API from './API';

import { BrowserRouter as Router } from 'react-router-dom';
import { Route, useHistory, Switch, Redirect } from 'react-router-dom';

const App = () => {

  // Need to place <Router> above the components that use router hooks
  return (
    <Router>
      <Main></Main>
    </Router>
  );

}

function Main() {
  const [memeList, setMemeList] = useState([]);
  const [templateList, setTemplateList] = useState([]);
  const [dirty, setDirty] = useState(true);
  const [loggedIn, setLoggedIn] = useState(false); // at the beginning, no user is logged in
  const [user, setUser] = useState(null);
  const [message, setMessage] = useState('');
  const [currentMeme, setCurrentMeme] = useState(undefined);

  const routerHistory = useHistory();

  // Get templates and memes from server when needed
  useEffect(() => {
    if (dirty) {
      API.getMemes(loggedIn)
         .then(memes => {
            setMemeList(memes);
            setDirty(false);
          })
         .catch(e => handleErrors(e));

      API.getTemplates()
         .then(templates => {
            setTemplateList(templates);
          })
         .catch(e => handleErrors(e));
    }
  }, [dirty, loggedIn]);

  // show error message in toast
  const handleErrors = (err) => {
    if (err.error)
      setMessage(err.error);
    else if (err.message)
      setMessage(err.message);
    else
      setMessage('Something went wrong');
  }

  // create a new meme
  const handleCreate = (meme) => {
    return new Promise( (resolve, reject) => {
      API.addMeme(meme)
         .then(() => {
              resolve();
              setDirty(true);
          })
         .catch(e => reject(e)); // error is handled and visualized in the creationView modal, do not manage error, throw it
    });
  }

  // delete a meme
  const handleDelete = (meme) => {
    // Promise needed in MGCollectionViewCell to avoid infinite loading animation
    // of delete button if an error occurs (e.g. server not responding)
    return new Promise ( (resolve, reject) => {
      API.deleteMeme(meme)
      .then(() => setDirty(true))
      .catch(e => {
        handleErrors(e);
        reject(e);
      });
    });
  }

  // create a copy of an existing meme
  const handleCopy = (meme) => {
    if (loggedIn && user) {
      const copy = { ...meme };
      setCurrentMeme(copy);
      routerHistory.push('/copy-meme');
    }
  }

  // open detail view for selected meme
  const handleDetail = (meme) => {
      const copy = { ...meme };
      setCurrentMeme(copy);
      routerHistory.push('/meme-detail');
  }

  const doLogIn = async (credentials) => {
    try {
      const user = await API.logIn(credentials);
      setUser(user);
      setLoggedIn(true);
      setMemeList([]);
      setDirty(true);
      setMessage(`Welcome, ${user.name}!`);
    }
    catch (err) {
      // error is handled and visualized in the login form, do not manage error, throw it
      throw err;
    }
  }

  const handleLogOut = async () => {
    API.logOut()
       .then( () => {
        // clean up everything
        setLoggedIn(false);
        setUser(null);
        setMemeList([]);
        setDirty(true);
       })
       .catch(e => handleErrors(e));
  }

  // check if user is authenticated
  useEffect(() => {
    const checkAuth = async () => {
      try {
        // here you have the user info, if already logged in
        const user = await API.getUserInfo();
        setUser(user);
        setLoggedIn(true);
      } catch (err) {
        console.log(err.error); // mostly unauthenticated user
      }
    };
    checkAuth();
  }, []);

  return (
    <>
      <MGNavbar routerHistory={routerHistory} onLogOut={handleLogOut} loggedIn={loggedIn}  user={user} errorMessage={message} />
      
      <Container className="main-container vh-100" fluid>
        <div style={{top: '1vh', zIndex: '2', position: 'relative'}}>
          <Toast className='toast' show={message} onClose={() => setMessage('')} delay={3000} autohide animation={false} >
            <Toast.Header>
              <strong className="pr-2 mr-auto">MƎMƎGenerat<FontAwesomeIcon icon={faGrinSquintTears} size="xs" />r</strong>
              <small className='pl-2'>just now</small>
            </Toast.Header>
            <Toast.Body>{message}</Toast.Body>
          </Toast>
        </div>

        <Row className="justify-content-center"><Form.Label className="App-title mt-3">Unleash your creativity</Form.Label></Row>
        <Row className="justify-content-center"><Form.Label className="App-subtitle mt-1 mb-4">Generate your memes and make your friends laugh</Form.Label></Row>

        { loggedIn && !dirty && <div className="relative"><Button className='Button-Create' variant="dark" onClick={ () => routerHistory.push('/new-meme') }>Create new meme</Button></div> }
        <Row className="justify-content-center"><MGCollectionView dirty={dirty} memes={memeList} templates={templateList} loggedIn={loggedIn} user={user} onDelete={handleDelete} onCopy={handleCopy} onDetail={handleDetail}/></Row>

        <Switch>
          <Route path="/login">
            { loggedIn ?  <Redirect to="/" />  : <LoginForm login={doLogIn} hide={() => routerHistory.push('/')} />}
          </Route>
          <Route path="/new-meme">
            { loggedIn ? (templateList.length > 0 && <MGCreationView hide={() => routerHistory.push('/')} templates={templateList} meme={undefined} creator={user} handleCreate={handleCreate}/>)
                       : <Redirect to="/" /> 
            }
          </Route>
          <Route path="/copy-meme">
            { loggedIn ? (templateList.length > 0 && <MGCreationView hide={() => routerHistory.push('/')} templates={templateList} meme={currentMeme} creator={user} handleCreate={handleCreate}/>)
                       : <Redirect to="/" /> 
            }
          </Route>
          <Route path='/meme-detail'>
            <MGDetailView hide={() => routerHistory.push('/')} templates={templateList} meme={currentMeme}></MGDetailView>
          </Route>
        </Switch>

      </Container>
    </>
  );
}

export default App;
