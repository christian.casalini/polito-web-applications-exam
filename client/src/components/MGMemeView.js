import 'bootstrap/dist/css/bootstrap.min.css';
import '../App.css';
import './MGMemeView.css'
import { Image, Form } from 'react-bootstrap';


function MGMemeView(props) {
    let textAreas = [];

    if (!(props.meme && props.template ))
        return '';

    if (props.meme.text1.length)
        textAreas.push({ text: props.meme.text1, pos: props.template.text1_pos });

    if (props.meme.text2.length && props.template.texts_n > 1)
        textAreas.push({ text: props.meme.text2, pos: props.template.text2_pos });

    if (props.meme.text3.length && props.template.texts_n > 2)
        textAreas.push({ text: props.meme.text3, pos: props.template.text3_pos });

    return (
        <div style={{ position: 'relative' }}>
            <Image className="hover-shadow" src={props.template.source} style={{ width: "380px" }} />
            {textAreas.map((t) =>
                <Form.Label key={t.pos} className={t.pos} style={{ fontFamily: props.meme.font, color: props.meme.text_color }}>{t.text}</Form.Label>
            )}
        </div>
    );
}

export default MGMemeView;