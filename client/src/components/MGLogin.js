import { Form, Button, Alert, Modal, Spinner } from 'react-bootstrap';
import { useState } from 'react';

function LoginForm(props) {
  const [username, setUsername] = useState('user2@test.it');
  const [password, setPassword] = useState('esame2memegitPr0bitU2');
  const [errorMessage, setErrorMessage] = useState('');
  const [dirty, setDirty] = useState(false);

  function validateEmail(email) {
    let re = /\S+@\S+\.\S+/;
    return re.test(email);
  }

  const handleSubmit = (event) => {
    event.preventDefault();
    setErrorMessage('');
    const credentials = { username, password };

    // basic validation
    let valid = true;
    if (!username || !password || password.length < 8) {
      valid = false;
      setErrorMessage('Email cannot be empty and password must be at least 8 characters long.');
    } else if (!validateEmail(username)) {
      valid = false;
      setErrorMessage('Email has an invalid format.');
    }

    if(valid) {
      setDirty(true);
      props.login(credentials)
           .catch( err => {
             setErrorMessage(err.message);
             setDirty(false);
           } );
    }
  };

  const onHide = () => {
    if (!dirty)
      props.hide();
  }

  return (
    <Modal centered show animation={false} keyboard="true" onHide={() => onHide()}>
      <Form onSubmit={handleSubmit}>
        <Modal.Header>
          <Modal.Title>Login</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          { errorMessage && <Alert dismissible onClose={() => setErrorMessage('')} variant="danger"> {errorMessage} </Alert> }
          <Form.Group controlId="username">
            <Form.Label className='form-title'>Email</Form.Label>
            <Form.Control type="email" value={username} onChange={(ev) => setUsername(ev.target.value)} disabled={dirty} />
          </Form.Group>
          <Form.Group controlId="password">
            <Form.Label className='form-title'>Password</Form.Label>
            <Form.Control type="password" value={password} onChange={(ev) => setPassword(ev.target.value)} disabled={dirty} />
          </Form.Group>
        </Modal.Body>
        <Modal.Footer>
          <Button type="submit" disabled={dirty}>
          { dirty && <Spinner className='mr-2' as="span" animation="border" size="sm" role="status" aria-hidden="true" />}
            Login{ dirty && '...' }
          </Button>
        </Modal.Footer>
      </Form>
    </Modal>
  );
}

export { LoginForm };
