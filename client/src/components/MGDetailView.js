import 'bootstrap/dist/css/bootstrap.min.css';
import '../App.css';
import './MGCollectionViewCell.css';
import MGMemeView from './MGMemeView';
import { Modal, Container, Form, Row, Col, Button } from 'react-bootstrap';
import { Redirect } from 'react-router-dom';


function MGDetailView(props) {

    return (
        <> 
        {
        props.meme ? <Modal centered onHide={() => props.hide()} animation={false} keyboard="true" size='lg' show>
                        <Modal.Header>
                            <Modal.Title>Meme details</Modal.Title>
                        </Modal.Header>
                        <Modal.Body>
                            <Row>
                                <Col>
                                    <Container fluid style={{ position: 'relative', width: '420px' }}>
                                        <MGMemeView meme={props.meme} template={props.templates.find(t => t.id === props.meme.template_id)} />
                                    </Container>
                                </Col>
                                <Col>
                                    <Row className='ml-3 mb-3'>
                                        <Form.Label className='form-title mr-2'>Title: </Form.Label>
                                        <Form.Label>{props.meme.title}</Form.Label>
                                    </Row>
                                    <Row className='ml-3 mb-3'>
                                        <Form.Label className='form-title mr-2'>Text font: </Form.Label>
                                        <Form.Label>{props.meme.font}</Form.Label>
                                    </Row>
                                    <Row className='ml-3 mb-3'>
                                        <Form.Label className='form-title mr-2'>Text color: </Form.Label>
                                        <Form.Label >{props.meme.text_color}</Form.Label>
                                        <Button className='mx-3' style={{ width: '1.5em', height: '1.5em', borderColor: '#000000', backgroundColor: props.meme.text_color }} />
                                    </Row>
                                    <Row className='ml-3 mb-3'>
                                        <Form.Label className='form-title mr-2'>Creator: </Form.Label>
                                        <Form.Label>{props.meme.creator_name}</Form.Label>
                                    </Row>
                                    <Row className='ml-3'>
                                        <Form.Label className='form-title mr-2'>Visibility: </Form.Label>
                                        <Form.Label>{props.meme.private ? 'Private' : 'Public'}</Form.Label>
                                    </Row>
                                </Col>
                            </Row>
                        </Modal.Body>
                    </Modal>
                : <Redirect to="/" />
        } 
        </>
    );
}

export default MGDetailView;