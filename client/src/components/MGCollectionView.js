import 'bootstrap/dist/css/bootstrap.min.css';
import '../App.css';
import './MGCollectionViewCell.css';
import { Container, Row, Col } from 'react-bootstrap';
import MGCollectionViewCell from './MGCollectionViewCell';


function MGCollectionView(props) {
    const memes = [...props.memes].reverse();
    const loadingMemes = [1, 2, 3, 4, 5, 6, 7, 8, 9]; // Tried on a 4K monitor, 9 placeholders do cover the entire height
    const loading = props.dirty || !props.templates || props.templates.length === 0;

    return (
            <Container className='vh-100' fluid style={{backgroundColor: 'white'}}>
                {   loading ?
                        (<Row className="CollectionView-Content mt-5">
                            {loadingMemes.map( (loadM) => 
                                <Col key={`loadCol-${loadM}`} xl={4} lg={6} md={12} className="mb-5">
                                    <Container className='loading-cell' fluid key={`loadCell-${loadM}`} ></Container>
                                </Col>
                             )}
                        </Row>)
                    :
                        (<Row className="CollectionView-Content mt-5">
                            {memes.map((m) => 
                                <Col key={`memeCol-${m.id}`} xl={4} lg={6} md={12} className="mb-5">
                                    <MGCollectionViewCell key={`memeCell-${m.id}`} meme={m} templates={props.templates} loggedIn={props.loggedIn} user={props.user} onDelete={props.onDelete} onCopy={props.onCopy} onDetail={props.onDetail}/>
                                </Col> 
                            )}
                        </Row>)
                }
            </Container>
    );
}

export default MGCollectionView;
