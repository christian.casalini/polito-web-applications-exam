import 'bootstrap/dist/css/bootstrap.min.css';
import '../App.css';
import './MGCollectionViewCell.css';

import { Modal, Button, Container, Row, Col, FormControl, Form, Image, Alert, Spinner } from 'react-bootstrap';
import MGMemeView from './MGMemeView';
import { useState } from 'react';
import { defaultColors, defaultFonts } from './MGConsts';
import { Route, Switch } from 'react-router';
import { useRouteMatch } from 'react-router-dom';

function MGCreationView(props) {
    const emptyMeme = {
        title: '',
        parent_id: -1,
        creator_id: props.creator.id,
        text_color: defaultColors[1],
        font: defaultFonts[0],
        private: 0,
        text1: '',
        text2: '',
        text3: '',
        template_id: props.templates[0].id
    };

    const [meme, setMeme] = useState(props.meme ? props.meme : emptyMeme);
    const [template, setTemplate] = useState(props.templates.find( t => t.id === meme.template_id ));
    const [errorMessage, setErrorMessage] = useState('');
    const [dirty, setDirty] = useState(false);

    const isCopy = useRouteMatch('/copy-meme');
    const lockVisibility = (isCopy && props.meme.creator_id !== props.creator.id && props.meme.private);
    let disableText2 = template.texts_n < 2;
    let disableText3 = template.texts_n < 3;

    function handleSubmit() {
        // If it is a copy, set current user's id as creator_id and set original_meme's id as parent_id
        if (isCopy) {
            meme.parent_id = meme.id;
            meme.creator_id = props.creator.id;
        }

        // As we keep text on template changes for better UX, be sure that when submitting there's no textField
        // still filled for a template that does not allow it (since it's not transparent to the user)
        if (disableText2 && meme.text2.length > 0)
            meme.text2 = '';

        if (disableText3 && meme.text3.length > 0)
            meme.text3 = '';
        
        // Do some validation 
        let valid = true;
        if (!meme.title) {
            valid = false;
            setErrorMessage('The title cannot be empty.');
        } else if (!meme.text1 && !meme.text2 && !meme.text3) {
            valid = false;
            setErrorMessage('At least one text field must be filled.');
        } else if (meme.title.length > 35) {
            valid = false;
            setErrorMessage('Title cannot be longer than 35 characters.');
        } else if (meme.text1.length > 200 || meme.text2.length > 200 || meme.text3.length > 200) {
            valid = false;
            setErrorMessage('A text field cannot contain more than 200 characters.');
        }

        if (valid) {
            setDirty(true);
            props.handleCreate(meme)
                 .then( () => props.hide() )
                 .catch( e => {
                     setErrorMessage(e.error);
                     setDirty(false);
                 });
        }
    }

    function updateTitle(newTitle) {
        const newMeme = { ...meme }
        newMeme.title = newTitle;
        setMeme(newMeme);
    }

    function updateText1(newText1) {
        const newMeme = { ...meme }
        newMeme.text1 = newText1;
        setMeme(newMeme);
    }

    function updateText2(newText2) {
        const newMeme = { ...meme };
        newMeme.text2 = newText2;
        setMeme(newMeme);
    }

    function updateText3(newText3) {
        const newMeme = { ...meme };
        newMeme.text3 = newText3;
        setMeme(newMeme);
    }

    function updateFont(newFont) {
        const newMeme = { ...meme };
        newMeme.font = newFont;
        setMeme(newMeme);
    }

    function updateColor(newColor) {
        const newMeme = { ...meme };
        newMeme.text_color = newColor;
        setMeme(newMeme);
    }

    function updateTemplate(newTemplate) {
        const newMeme = { ...meme };
        newMeme.template_id = newTemplate.id;
        setMeme(newMeme);
        setTemplate(newTemplate);
        disableText2 = template.texts_n < 2;
        disableText3 = template.texts_n < 3;
    }

    function updateIsPrivate(isPrivate) {
        const newMeme = { ...meme };
        newMeme.private = isPrivate;
        setMeme(newMeme);
    }

    function handleHide() {
        if (!dirty)
            props.hide();
    }

    return (
        <Modal onHide={ () => handleHide() } animation={false} keyboard={!dirty} show centered size='lg' >

            <Modal.Header >
                <Modal.Title>Create new Meme</Modal.Title>
            </Modal.Header>

            <Modal.Body style={{ backgroundColor: 'white' }}>
                <Container fluid>
                    { errorMessage && <Alert dismissible onClose={() => setErrorMessage('')} variant="danger"> {errorMessage} </Alert> }
                    <Row>
                        <Col>
                            <Row ><Form.Label className='form-title'>Title</Form.Label></Row>
                            <Row ><FormControl required type='input' placeholder="Title" value={meme.title} onChange={ev => updateTitle(ev.target.value)} /></Row>
                            <Row className='mt-3'><MGMemeView meme={meme} template={template}/></Row>
                        </Col>
                        <Col>

                            <Row className='mx-1'><Form.Label className='form-title'>Texts</Form.Label></Row>
                            <Row className='mx-1'><FormControl className="mb-1" type='input' as='textarea' placeholder="Text 1" value={meme.text1} onChange={ev => updateText1(ev.target.value)} maxLength={200} /></Row>
                            <Row className='mx-1'><FormControl className="my-1" type='input' as='textarea' placeholder="Text 2" value={disableText2 ? '' : meme.text2} disabled={disableText2} onChange={ev => updateText2(ev.target.value)} maxLength={200}  /></Row>
                            <Row className='mx-1'><FormControl className="my-1" type='input' as='textarea' placeholder="Text 3" value={disableText3 ? '' : meme.text3} disabled={disableText3} onChange={ev => updateText3(ev.target.value)} maxLength={200} /></Row>

                            <Row className='mx-1 mt-3'>
                                <Col>
                                    <Row >
                                        <Form.Label className='align-self-start form-title'>Color</Form.Label>
                                    </Row>

                                    <Row>
                                        {defaultColors.map((c) =>
                                            <Button key={`btn-${c}`} className='mx-1' onClick={() => updateColor(c)} style={{ width: '1.5rem', height: '1.8rem', borderColor: '#000000', backgroundColor: c }} />
                                        )}
                                    </Row>
                                </Col>
                                <Col md={4}>
                                    <Row className='mx-3'><Form.Label className='align-self-start mx-1 form-title'>Visibility</Form.Label></Row>
                                    <Row className='mx-3'>
                                        <Form.Check className='mx-1' label="Private" disabled={lockVisibility} defaultChecked={meme.private} onChange={(ev) => updateIsPrivate(ev.target.checked ? 1 : 0)} key={`inline-checkbox-1`} style={{ color: 'black' }} />
                                    </Row>
                                </Col>
                            </Row>

                            <Row className='mx-1 mt-4'><Form.Label className='form-title'>Font</Form.Label></Row>
                            <Row className="justify-content-left"><Col md={10}>
                                <Form.Control defaultValue={meme.font} className="mx-1" as="select" onChange={ev => updateFont(ev.target.value)}>
                                    {defaultFonts.map((f) =>
                                        <option key={`font-${f}`} value={f}>{f}</option>
                                    )}
                                </Form.Control>
                            </Col>

                            </Row>
                        </Col>
                    </Row>

                    <Switch>
                        <Route path='/new-meme'>
                            <Row className='mt-4' style={{ display: 'flex', flexWrap: 'nowrap', overflowX: 'auto', height: '14vh', backgroundColor: 'rgba(85, 127, 195, 0.0)' }}>
                                {props.templates.map((t) =>
                                    <Image key={`template-${t.id}`} className="mx-1" src={t.source} style={{height: '12vh', flex: '0 0 auto', cursor: 'pointer'}} onClick={() => updateTemplate(t)}></Image>
                                )}
                            </Row>
                        </Route>
                        <Route path='/copy-meme'></Route>
                    </Switch>
                    
                </Container>
            </Modal.Body>

            <Modal.Footer>
                <Button variant="danger" onClick={() => props.hide()} disabled={dirty} >Discard</Button>
                <Button variant="primary" onClick={() => handleSubmit()} disabled={dirty} >
                    { dirty && <Spinner className='mr-2' as="span" animation="border" size="sm" role="status" aria-hidden="true" />}
                    Add{ dirty && '...' }
                </Button>
            </Modal.Footer>

        </Modal>
    );
}

export default MGCreationView;