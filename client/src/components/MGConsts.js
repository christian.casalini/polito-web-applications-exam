const defaultColors = ['#000000', '#ffffff', '#fcba03', '#5ce074', '#5d88d9'];
const defaultFonts = ['Helvetica', 'Times New Roman', 'Courier New', 'Bradley Hand', 'Brush Script MT', 'Cursive'];


export {defaultColors, defaultFonts}