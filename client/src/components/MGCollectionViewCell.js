import 'bootstrap/dist/css/bootstrap.min.css';
import './MGCollectionViewCell.css'
import { Form, Row, Container, Spinner } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTrash, faCopy, faEye, faEyeSlash } from '@fortawesome/free-solid-svg-icons'
import MGMemeView from './MGMemeView';
import { useState } from 'react';


function MGCollectionViewCell(props) {
    const [dirty, setDirty] = useState(false);

    const ownMeme = props.user && props.user.id === props.meme.creator_id;
    const visIcon = props.meme.private ? faEyeSlash : faEye;
    const visIconClass = props.meme.private ? 'vis-private' : 'vis-public';
    const deleteIconClass = (props.loggedIn && ownMeme) ? 'delete-enabled' : 'delete-disabled';
    const template = props.templates.find( t => t.id === props.meme.template_id );

    const onDelete = () => {
        if (props.loggedIn && ownMeme) {
            setDirty(true);
            props.onDelete(props.meme)
                 .catch( () => setDirty(false) );
        }
    }

    const onCopy = () => {
        if (props.loggedIn)
            props.onCopy(props.meme);
    }

    return (
        <Container className='cell-container' fluid>
            <Row className='justify-content-center pt-2'><Form.Label className="meme-title">{props.meme.title}</Form.Label></Row>
            <Row className='justify-content-center mt-1' onClick={ () => props.onDetail(props.meme) }><MGMemeView meme={props.meme} template={template}/></Row>
            <Row className='justify-content-center mt-2 pb-2'>
                <div className='cell-footer'>
                    <Form.Text className="credits">Created by {props.meme.creator_name}</Form.Text>
                    { props.loggedIn &&
                        <>
                        <FontAwesomeIcon className={visIconClass} icon={visIcon} size="1x" />
                        <FontAwesomeIcon className='copy-enabled' icon={faCopy} size="1x" onClick={ () => onCopy() }/>
                        {   dirty ?
                            <Spinner className={deleteIconClass} animation="border" variant="danger" size='sm'/>
                            :
                            <FontAwesomeIcon className={deleteIconClass} icon={faTrash} size="1x" onClick={ () => onDelete() }/>
                        }
                        </>
                    }
                </div>
            </Row>
        </Container>
    );
}

export default MGCollectionViewCell;