import 'bootstrap/dist/css/bootstrap.min.css';
import '../App.css';
import { Navbar, Nav, Dropdown, Spinner } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faGrinSquintTears, faUserCircle } from '@fortawesome/free-solid-svg-icons'
import { Col, Row } from 'react-bootstrap';
import { useState, useEffect } from 'react';

function MGNavbar(props) {
  const { routerHistory, onLogOut, loggedIn, user, errorMessage } = props;
  const [dirty, setDirty] = useState(false);

  const showLoginForm = () => {
    routerHistory.push('/login');
  }

  const doLogOut = () => {
    setDirty(true);
    onLogOut();
  }

  // We need to know when a login/logout has successfully completed or if it failed
  // in order to know when to re-set dirty to false
  useEffect(() => {
    setDirty(false);
  }, [loggedIn, errorMessage]);

  return (
    <Navbar bg="dark" variant="dark">
      <Col >
        <Navbar.Brand href="/" className="ms-3">
          MƎMƎGenerat<FontAwesomeIcon icon={faGrinSquintTears} size="xs" />r
        </Navbar.Brand>
      </Col>

      <Col >
        <Row className='justify-content-end'>
          { loggedIn ? <Nav><Nav.Item className="mt-2" style={{color: 'whitesmoke'}}>{user.name}</Nav.Item></Nav> 
                     : <Nav className="mx-3"><Nav.Link eventKey={2} onClick={showLoginForm}>Login</Nav.Link></Nav> }

          { loggedIn && dirty &&  
                      <Nav className="text-danger" >
                        <Nav.Item className="mt-2 mx-2">
                          <Spinner className='mx-2' as="span" animation="border" size="sm" role="status" aria-hidden="true" />
                          Logout...
                        </Nav.Item>
                      </Nav>
          }

          { loggedIn && !dirty && 
                        <Nav.Item>
                          <Dropdown variant="dark" >
                            <Dropdown.Toggle className='ml-2' id="dropdown-basic" variant="dark">
                              <FontAwesomeIcon icon={faUserCircle} size="lg" /> {props.username}
                            </Dropdown.Toggle>

                            <Dropdown.Menu alignRight className='me-4'>
                              <Dropdown.Item className="text-danger" active={false} onClick={() => doLogOut()}>Logout</Dropdown.Item>
                            </Dropdown.Menu>
                          </Dropdown>
                        </Nav.Item> 
          }
        </Row>
      </Col>
    </Navbar>
  );
}

export default MGNavbar;
